﻿using NUnit.Framework;
using Fourth_Automation1.PageObjectModel;

namespace Fourth_Automation1.Tests
{
    [TestFixture]
   public class LeaveRequest:Base
    {
        private const string fromDate = "Tue, 25 June 2020";
        private const string toDate = "Wed, 24 June 2020";
        private const int leaveTypePosition = 5;
        
        [SetUp]
        public void Setup()
        {
            HRMApp.LoginPage.RoleButton.Click();
            HRMApp.LoginPage.RoleNames[2].Click();
            HRMApp.DashBoardPage.WaitForMenuProfile();
            HRMApp.DashBoardPage.LeaveModule.Click();
            HRMApp.ApplyLeavePage.ApplyLink.Click();
            HRMApp.ApplyLeavePage.WaitForApplicationForm();
        }

        [Test]
        public void GivenValidInputs_WhenApplyingForLeave_ThenDisplaySuccessMessage()
        {
            HRMApp.ApplyLeavePage.SelectLeaveType(leaveTypePosition);
            HRMApp.ApplyLeavePage.DatePickerIcons[0].Click();
            HRMApp.ApplyLeavePage.TodayButton.Click();
            HRMApp.ApplyLeavePage.WaitForAttachmentButton();
            HRMApp.ApplyLeavePage.ApplyButton.Click();
            HRMApp.ApplyLeavePage.WaitForSuccessMessage();

            Assert.IsTrue(HRMApp.ApplyLeavePage.SuccessMessage.Displayed);
        }

        [Test]
        public void GivenInValidInput_WhenApplyingForLeave_ThenDisplayValidationMessages()
        {
            HRMApp.ApplyLeavePage.ApplyButton.Click();
            HRMApp.ApplyLeavePage.WaitForValidationMessages();

            Assert.IsTrue(HRMApp.ApplyLeavePage.ValidationMessages[18].Displayed);
        }
        
        [Test]
        public void GivenIncorrectDates_WhenApplyingForLeave_ThenDisplayValidationMessages()
        {
            HRMApp.ApplyLeavePage.SelectLeaveType(leaveTypePosition);
            HRMApp.ApplyLeavePage.ToDate.SendKeys(toDate);
            HRMApp.ApplyLeavePage.WaitForAttachmentButton();
            HRMApp.ApplyLeavePage.FromDate.Clear();
            HRMApp.ApplyLeavePage.FromDate.SendKeys(fromDate);
            HRMApp.ApplyLeavePage.ApplyButton.Click();
            HRMApp.ApplyLeavePage.WaitForDateValidationMessage();

            Assert.IsTrue(HRMApp.ApplyLeavePage.ValidationMessages[11].Displayed);
        }
    }
}

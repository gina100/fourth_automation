﻿using NUnit.Framework;
using Fourth_Automation1.PageObjectModel;

namespace Fourth_Automation1.Tests
{
    [TestFixture]
   public class JobTitle : Base
    {
        private string jobTitleName = "Senior Automation Tester";
        private string jobDescription = "Automation Tester";
        
        [SetUp]
        public void Setup()
        {
            HRMApp.LoginPage.RoleButton.Click();
            HRMApp.LoginPage.RoleNames[0].Click();
            HRMApp.DashBoardPage.WaitForMenuProfile();
            HRMApp.JobPage.AdminLink.Click();
            HRMApp.JobPage.JobLink.Click();
            HRMApp.JobPage.JobTitleLink.Click();
            HRMApp.JobPage.WaitForJobTitleContainer();
            HRMApp.JobPage.JobTitleAddBtn.Click();
            HRMApp.JobPage.WaitForJobTitleModal();
        }

        [Test]
        public void GivenValidInputs_WhenAddingJobTitle_ThenDisplaySuccessMessage()
        {
            HRMApp.JobPage.JobTitleName.SendKeys(jobTitleName);
            HRMApp.JobPage.JobDescription.SendKeys(jobDescription);
            HRMApp.JobPage.SaveBtn.Click();
            HRMApp.JobPage.WaitForSuccessMessage();

            Assert.IsTrue(HRMApp.JobPage.SuccessMessage.Displayed);
        }

        [Test]
        public void GivenInValidInput_WhenAddingJobTitle_ThenDisplayValidationMessage()
        {
            HRMApp.JobPage.SaveBtn.Click();
            HRMApp.JobPage.WaitForValidationMessage();

            Assert.IsTrue(HRMApp.JobPage.ValidationMessage.Displayed);
        }
    }
}

﻿using OpenQA.Selenium;

namespace Fourth_Automation1.PageObjectModel
{
    public class JobTitlesPage:BasePage
    {
        public JobTitlesPage(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement AdminLink => PageRoot.FindElement(By.Id("menu_admin_viewAdminModule"));
        public IWebElement JobLink => PageRoot.FindElement(By.CssSelector("#menu_admin_Job > a"));
        public IWebElement JobTitleLink => PageRoot.FindElement(By.CssSelector("a#menu_admin_viewJobTitleList"));
        public IWebElement JobTitleContainer => PageRoot.FindElement(By.Id("jobTitlesDiv"));
        public IWebElement JobTitleAddBtn => JobTitleContainer.FindElement(By.CssSelector("div > a"));
        public IWebElement JobTitleModal => PageRoot.FindElement(By.Id("modal1"));
        public IWebElement JobTitleName => JobTitleModal.FindElement(By.Id("jobTitleName"));
        public IWebElement JobDescription => JobTitleModal.FindElement(By.Id("jobDescription"));
        public IWebElement SaveBtn => JobTitleModal.FindElement(By.CssSelector("a.btn"));
        public IWebElement ValidationMessage => JobTitleModal.FindElement(By.CssSelector("span"));
        public IWebElement SuccessMessage => Driver.FindElement(By.CssSelector("div.toast-message"));

        public void WaitForJobTitleContainer()
        {
            Wait.Until(driver => JobTitleContainer.Displayed);
        }

        public void WaitForJobTitleModal()
        {
            Wait.Until(driver => JobTitleModal.Displayed);
            Wait.Until(driver => JobDescription.Displayed);
            Wait.Until(driver => JobTitleName.Displayed);
        }
        
        public void WaitForSuccessMessage()
        {
            Wait.Until(driver => SuccessMessage.Displayed);
        }

        public void WaitForValidationMessage()
        {
            Wait.Until(driver => ValidationMessage.Displayed);
        }
    }
}

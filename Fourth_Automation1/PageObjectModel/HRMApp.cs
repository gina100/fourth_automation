﻿using OpenQA.Selenium;

namespace Fourth_Automation1.PageObjectModel
{
    public static class HRMApp
    {
        private const string url = "https://orangehrm-demo-6x.orangehrmlive.com/";
        public static LoginPage LoginPage;
        public static DashBoardPage DashBoardPage;
        public static LeavePage ApplyLeavePage;
        public static JobTitlesPage JobPage;
        public static void Init(IWebDriver driver)
        {
            driver.Navigate().GoToUrl(url);
            LoginPage = new LoginPage(driver);
            DashBoardPage = new DashBoardPage(driver);
            ApplyLeavePage = new LeavePage(driver);
            JobPage = new JobTitlesPage(driver);
        }
    }
}

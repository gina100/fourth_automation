﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Fourth_Automation1.PageObjectModel
{
    public class BasePage
    {
        private const int WaitTime = 15;
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        public BasePage(IWebDriver driver)
        {
            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(WaitTime));
            this.Driver = driver;
        }

        public IWebElement PageRoot => Driver.FindElement(By.CssSelector("body"));
    }
}

﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Fourth_Automation1.PageObjectModel
{
   public class LeavePage:BasePage
    {
        public LeavePage(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement applicationForm => PageRoot.FindElement(By.Id("applyLeaveForm"));
        private IWebElement attachmentButton => applicationForm.FindElement(By.CssSelector("div.btn input"));
        public IWebElement ApplyLink => PageRoot.FindElement(By.Id("menu_leave_applyLeave"));
        public IWebElement LeaveType => applicationForm.FindElement(By.CssSelector("#leaveType_inputfileddiv"));
        public IWebElement LeaveTypeDropdown=> LeaveType.FindElement(By.CssSelector("ul"));
        public IList<IWebElement> LeaveTypeOptions=> LeaveTypeDropdown.FindElements(By.CssSelector("li"));
        public IWebElement FromDate => applicationForm.FindElement(By.Id("from"));
        public IWebElement ToDate => applicationForm.FindElement(By.Id("to"));
        public IList<IWebElement> DatePickerIcons => applicationForm.FindElements(By.CssSelector("i"));
        public IWebElement TodayButton => applicationForm.FindElement(By.CssSelector("div.picker__footer > button"));
        public IWebElement ApplyButton => applicationForm.FindElement(By.CssSelector("div.row div button.btn"));
        public IWebElement SuccessMessage => Driver.FindElement(By.CssSelector("#toast-container div.toast-message"));
        public IList<IWebElement> ValidationMessages => applicationForm.FindElements(By.CssSelector("span"));
        
        public void WaitForApplicationForm()
        {
            Wait.Until(driver => applicationForm.Displayed);
        }

        public void WaitForSuccessMessage()
        {
            Wait.Until(driver => SuccessMessage.Displayed);
        }

        public void SelectLeaveType(int typePosition)
        {
            LeaveType.Click();
            Wait.Until(driver => LeaveTypeOptions.Count > 0);
            LeaveTypeOptions[typePosition].Click();
        } 

        public void WaitForDateValidationMessage()
        {
            Wait.Until(driver => ValidationMessages[11].Displayed);
        }

        public void WaitForAttachmentButton()
        {
           Wait.Until(driver => attachmentButton);
        }

        public void WaitForValidationMessages()
        {
           Wait.Until(driver => ValidationMessages[8].Displayed);
           Wait.Until(driver => ValidationMessages[13].Displayed);
           Wait.Until(driver => ValidationMessages[18].Displayed);
        }
    }
}

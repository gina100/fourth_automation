﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Fourth_Automation1.PageObjectModel
{
    public class LoginPage: BasePage
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {

        }

        public IWebElement RoleButton => PageRoot.FindElement(By.Id("loginAsButtonGroup"));
        public IList<IWebElement> RoleNames => RoleButton.FindElements(By.CssSelector("a"));
    }
}

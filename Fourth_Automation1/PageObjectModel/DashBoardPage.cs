﻿using OpenQA.Selenium;

namespace Fourth_Automation1.PageObjectModel
{
   public class DashBoardPage:BasePage
    {
        public DashBoardPage(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement menuProfile => PageRoot.FindElement(By.Id("menu-profile"));
        public IWebElement LeaveModule => PageRoot.FindElement(By.CssSelector("#menu_leave_viewLeaveModule a"));
       
        public void WaitForMenuProfile()
        {
            Wait.Until(driver => menuProfile.Displayed);
        }
    }
}
